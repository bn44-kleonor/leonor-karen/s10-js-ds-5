/*============================================
ACTIVITY 

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

1) Create a recursive function called "sum" that displays that sum of consecutive numbers so that
console.log(sum(10)) will display 55:
1+2+3+4+5+6+7+8+9+10 = 55;

============================================*/
console.log('SUM ******************************');
function sum(num) {
	if(num !== 0) {
		return num + sum(num-1); 
	}
	return num;
}

console.log("Sum = " + sum(10));



/*=========================================== 
2) Create a recursive function called "sumOdd" that displays that sum of odd numbers so that
console.log(sum(50)) will display 625:
============================================*/

function sumOdd(num) {
	if(num > 1) {
		if(num%2 !== 0) {
			return num + sumOdd(num-2);
		}
		return sumOdd(num-1);
	}
	return num;
}

console.log("sumOdd = " + sumOdd(50));


/*=========================================== 
3) Create a recursive function called "sumEven" that displays that sum of even numbers so that
console.log(sum(50)) will display 650:
============================================*/

function sumEven(num) {
	if(num > 1) {
		if(num%2 === 0) {
			return num + sumEven(num-2);
		}
		return sumEven(num-1);
	}
	return num;
}

console.log("sumEven = " + sumEven(50));


console.log('FIBONACCI ******************************');

/*============================================
ACTIVITY 

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

/*============================================
STRETCH GOAL (Put in script4.js)

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

The Fibonacci Sequence (aka Golden Ratio) is the series of numbers:

0, 1, 1, 2, 3, 5, 8, 13 ...

The next number is found by adding up the two numbers before it.

2 is found by adding the two numbers before it (1+1)
3 is found by adding the two numbers before it (1+2),
5 is found by adding the two numbers before it (2+3),
8 is found by adding the two numbers before it (3+5),
13 is found by adding the two numbers before it (5+8),
and so on!

Another way to see it is shown below:

1. 0
2. 1
3. 1
4. 2
5. 3
6. 5
7. 8
8. 13

If you're looking for the 3rd number, it's 1.
If you're looking for the 6th number, it's 5.
If you're looking for the 8th number, it's 13.
and so on!

Create a function called fibonacci that gives the total of numbers up to the given value of the parameter.
eg. fibonacci(8) === 13.

============================================*/

// console.log('Fibonacci ******************************');

// let i = 0;

// function fibonacci(count){
//   console.log(count);
//   if(count !== 0) {
//     //i = i + (i+1);

//     temp = i;
//     i = i + temp;
//     i++;

//     return count + fibonacci(count-1);
//   } else {
//     return count;
//   }
// }

// console.log(fibonacci(8));

// count = 8;
// let fibonacci = 1;
// let temp1;
// let temp2;

// for(let i=2; i <= count; i++) {
//   if(i<=2){
//     fibonacci = fibonacci + i;
//     console.log("1st if " + fibonacci);
//     temp1 = i;
//     temp2 = fibonacci;
//     i++;
//     console.log("     i is = " + i);
//   } else {
//   //if(i > 3){
//     fibonacci = temp2 + temp1;
//     temp1 = temp2;
//     temp2 = fibonacci;
//     console.log("fibonacci is " + fibonacci);
//     //temp = fibonacci;
//     i++;
//     console.log("     i is = " + i);
//   //}
//   }
// }

// console.log(count);

/*=================================
2 is found by adding the two numbers before it (1+1)
3 is found by adding the two numbers before it (1+2),
5 is found by adding the two numbers before it (2+3),
8 is found by adding the two numbers before it (3+5),
13 is found by adding the two numbers before it (5+8),
and so on!

ref 0
1. 0
2. 1
3. 1
4. 2
5. 3
6. 5
7. 8
8. 13

ref 1
1. 1
2. 1
3. 2
4. 3
5. 5
6. 8
7. 13
8. 21


mathematical equation
fib(n) = fib(n-1) + fib(n-2);
fib(n) = 6;
fib(n-1) = term 5 hence it is 3.
fib(n-2) = term 4 hence it is 2.
3 + 2 = 5;
fib(6) = 5	//if fib sequence starts with 0
fib(6) = 8 //if fib sequence starts with 1


Try to apply this formula to fib(n) = 5;
fib(5) = fib(5-1) + fib(5-2);
fib(5) = fib(4) + fib(3);
fib(5) = 2 + 1;
fib(5) = 3;  //if fib sequence starts with 0
fib(5) = 5   //if fib sequence starts with 1


Try to apply this formula to fib(n) = 8;
fib(8) = fib(8-1) + fib(8-2);
fib(8) = fib(7) + fib(6);
fib(8) = 8 + 5;
fib(8) = 13 //if fib sequence starts with 0
fib(8) = 21 //if fib sequence starts with 1

==================================================*/
// series starts with '1'
console.log("====== Fibonacci series 1 ===========================")
function fibonacci(n){
	console.log(n);
	if(n <= 1) {
		return n;
	}
	return fibonacci(n-1) + fibonacci(n-2);		//maikli pero hindi efficient
}

// console.log(fibonacci(0));		//0
// console.log(fibonacci(1));		//1
// console.log(fibonacci(6));		//8
console.log("Fibonacci starting '1' = " + fibonacci(4));		//21



// series starts with '0'
console.log("====== Fibonacci series 0 ===========================")
function fibonacci(n){
	const result = [0,1];
	for(let i=2; i < n; i++) {			//for loop more efficient than recurssion
		result.push(result[i-1] + result[i-2]);
	}

	//return result
	if(result[result.length-1] <= 1) {
		return 0;
	} else {
		return result[result.length-1];
	}
}

console.log(fibonacci(0));		//0
console.log(fibonacci(1));		//1
console.log(fibonacci(8));		//13


