/*==========================================

Discussion 
WD004-S10-DATA-STRUCTURES-5

using 'While loop'


========================================== */


let x = [1, 5, 6, 11, 2];

let i = 0;
while(x[i] < 10) {
	console.log(x[i]);
	i++;
}


console.log("**** Arrays **************************");

let names = ['stella','mary','tesla','jake','pio','max'];

console.log(names[0]);	//stella
console.log(names[0][0]);	//s in stella
console.log(names[0].charAt(0));	//s in stella

console.log(names[0].charAt(names[0].length-1));	//s in stella


let z = 0;
while(names[z].charAt(0) !== 'p') {
	console.log(names[z]);
	z++;
}


console.log('*** Object ***********');
/*
Display values of properties inside people object until the one with a value of null */

people = {
0: 'a',
1: 'b',
2: 'c',
3: 'd',
4: 'e',
5: 'f',
6: 'g',
7: 'h',
8: 'i',
9: null,
10: 'j',
11: 'k'
};


i = 0;
while (people[i] !== null) {
	console.log(people[i]);
	i++;
}


console.log('**************');
/*
Display values of name property of students object until the one that is NOT less than 18 years old */

let students = [
{
name: 'Amy',
age: 15
},
{
name: 'Mark',
age: 16
},
{
name: 'Tess',
age: 19
},
{
name: 'Rico',
age: 14
}
];


i = 0;
while (students[i].age <= 18) {
	console.log(students[i].name);
	i++;
}









