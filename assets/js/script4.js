/*=====================================

DISCUSSION / ACTIVITY 
WD004-S10-DATA-STRUCTURES-5

LINKED LIST
A linear data structure where each element (aka __NODE__) is a separate __OBJECT__.
Each node has 2 properties: a ___VALUE___and a ___NEXT___ pointer.				//value carries the data, next points to the next node
____VALUE___holds data while ____NEXT____is a pointer/reference to the next node. 


The entry point into a linked list is the __HEAD___(1st pointer). The head is not a separate node but a pointer/reference to the __FIRST__node.
If the list is empty, then head is a ___NULL___reference.

The last node into a linked list has its next pointer set to ___NULL___.

Like an array, a linked list can contain different data types. 
However, unlike array, a linked list has ___NO___indexing. 
You have to ___TRAVERSE___the list from the head, in order, until you get to the node that you are looking for (i.e., value).

If a node has no next pointer pointing to it, it is ___ORPHANED___and removed from the list.


======================================*/

function LinkedList(){
	let Node = function(value){
		this.value = value;
		this.next = null;
	}

	this.head = null;

	this.getHead = function(){
		return this.head;
	}

	//push - add a new node to the end
	this.push = function(value){
		let node = new Node(value)				//instantiate a node before pushing value

		//if the list is empty/null (meaning the first push is the head)
		if(this.head == null) {					
			this.head = node;
			// console.log("display the 'node'*****");
			// console.log(node);
			// console.log("*****")
			// console.log(" ")
		} else {

			let currentNode = this.head;

			// console.log("this.head below*****")
			// console.log(this.head)
			// console.log("*****")
			// console.log(" ")


			// console.log("current node below*****")
			// console.log(currentNode)
			// console.log("*****")
			// console.log(" ")

			// console.log("this.value the 'node'*****");
			// console.log(this.value);
			// console.log("*****")
			// console.log(" ")

			// console.log("this.next below*****")
			// console.log(this.next)
			// console.log("*****")
			// console.log(" ")

			// console.log("this.next below*****")
			// console.log(this.push)
			// console.log("*****")
			// console.log(" ")


			// if(this.next == null){
			// 	this.next == currentNode;
			// 	console.log("display this.next 1 below")
			// 	console.log(this.next);
			// 	console.log("*****")
			// 	//this.push(currentNode) = this.next;
			// }

			// Node.next = this.next;
			// console.log("Node.next below*****")
			// console.log(Node.next)
			// console.log(Node.value)
			// console.log(Node)
			// console.log("*****")
			// console.log(" ")


			//while(currentNode.next == null) {
			while(currentNode.next != null) {
				// move the currentNode to the next node
				//currentNode.next = node;
				currentNode = currentNode.next;
				console.log("******** while*********")
			}


			// this.next = currentNode;
			// console.log(this.next);
			// console.log("node " + node);

			//once you have reached the LAST node, point the next pointer of the currentNode to the
			//new node that you just created
			currentNode.next = node;



		}
		return console.log(node);
		//return value;
		//return node;

		this.push(currentNode) = this.next;
		console.log("******** what???!!!!!!!**********")

	}
}


let pets = new LinkedList();
pets.push('kitten');
pets.push('puppy');
pets.push('kid');
console.log(pets.getHead());	//kitten
console.log(pets);


